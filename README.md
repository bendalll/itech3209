# itech3209
ITECH3209 Project 92

Test Server is: http://13.236.170.247:8010/

## Server Details

***OS:*** Ubuntu

***Virtual Environment Location***: /home/ubuntu/dev

***Django Site Location***: /home/ubuntu/dev/MyProject

***Custom uWSGI SystemD Service:*** uwsgi-myproject.service

***Software:***

- Ubuntu 16.06

- Django 2.0.5

- Python 3.6

- uWSGI 4.x


## How to Use CodeDeploy ##

 This BitBucket Repo is hooked up to the Test Server via AWS CodeDeploy. This basically means you can press a single button in this repository and it will trigger a series of actions on the server to refresh the code. This involves stopping the uWSGI service which is running Django, copying across the latest code from the ./MyDigitalHealth folder and then restarting uWSGI. If all goes well the test server will come up and display the new code. Assuming the new code works...

 - To Deploy the code, simply ***Commit*** the code to the repository
 
 - View your commit in the BitBucket repo web interface (*Commits* on the Left hand Menu)
 
 - Click the 'Deploy to AWS' link on the LHS menu bar.
 
 - Wait Patiently (you can view the deploy status in the EC2 console).
 
 - Go to http://13.236.170.247:8010/ and test the newly deployed code.
 
 
 ***NOTES:*** 
 
 Since we have people writing code in windows environments, only the `MyDigitalHealth/` folder is being copied by CodeDeploy.   
 
 The following files are related to CodeDeploy and shouldn't need to be touched unless you're trying to fix something that has broken with CodeDeploy: *after_install.sh, appspec.yml, before_install.sh, codedeploy_deploy.py*
 


## How to restart uWSGI ##

 Use the command `sudo systemctl restart uwsgi-myproject.service`
 
 To view the results of your command use `journalctl -xe` to display the systemctl log.



