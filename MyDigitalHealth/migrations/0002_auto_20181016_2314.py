# Generated by Django 2.1.2 on 2018-10-16 12:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('MyDigitalHealth', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='permission',
            old_name='card_package',
            new_name='package',
        ),
    ]
